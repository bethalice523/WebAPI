// ADD HEADERS
'use strict'

const restify = require('restify')
const server = restify.createServer()
const model = require('./model.js')

//const uri = 'mongodb://BethAlice:sampledb@ds031597.mlab.com:31597/sampledb'

server.use(restify.queryParser())

const defaultPort = 8000

server.use(restify.fullResponse())
server.use(restify.bodyParser())
server.use(restify.authorizationParser())

const port = process.env.PORT || defaultPort
server.listen(port, function(err) {
	if (err) {
		console.error(err)
	} else {
		console.log('App is ready at : ' + port)
	}
})

const htmlstatus = {'OK': 200,
										'created': 201}

/**
* route for getting a ship by its specified id from swapi api
* @param shipid		the id od the ship that is to be retrieved from swapi api
* @return		status 201 and the response body if the ship is found
* @return		error message if there the promise is rejected
*/
server.get('/ships/:shipid', function(req, res) {
	const shipid = req.params.shipid
	model.getShip(shipid, (err, response) => {
		res.setHeader('content-type', 'application/json')
		res.setHeader('accepts', 'GET')
		if(err) {
			res.send(err)
			res.end()
			throw new Error(err)
		} else {
			res.status(htmlstatus['OK'])
			res.send(response)
			res.end()
		}
	})
})

/**
* route for retrieving a page of ships from swapi api
* @param page		the page number that should be returned
* @param search		the name of the ship that is being searched for
* @return		status 201 and the response body if the page is found
* @return		status 201 and the response body if there are ships that satisfy the search
* @return		error message if there the promise is rejected
*/
server.get('/ships', function(req, res) {
	const page = req.params.page // curl -GET http://localhost:8000/ships?page=3
	const search = req.params.search // curl -GET http://localhost:8000/ships?search=r2
	model.allShip(page, search, (err, response) => {
		res.setHeader('content-type', 'application/json')
		res.setHeader('accepts', 'GET')
		if(err) {
			res.send(err)
			res.end()
		} else {
			res.status(htmlstatus['OK'])
			res.send(response)
			res.end()
		}
	})
})

/**
* route to get the fleet of ship saved
* @return		status 201 and the response body if the fleet is found
* @return		error message if there the promise is rejected
*/
server.get('/fleet', function(req, res) {
	model.viewfleet(req, (err, response) => {
		res.setHeader('content-type', 'application/json')
		res.setHeader('accepts', 'GET')
		if(err){
			res.send(err)
			res.end()
		} else {
			res.status(htmlstatus['OK'])
			res.send(response)
			res.end()
		}
	})
})

/**
* route to retrieve an individual ship from the fleet
* @param ship		the name of the ship to be retrieved
* @return		status 201 and the response body if the ship specified is found in the fleet
* @return		error message if there the promise is rejected
*/
server.get('/fleet/:shipsearch', function(req, res) {
	const ship = req.params.shipsearch
	model.viewindividual(ship, req, (err, response) => {
		res.setHeader('content-type', 'application/json')
		res.setHeader('accepts', 'GET')
		if(err){
			res.send(err)
			res.end()
		} else {
			res.status(htmlstatus['OK'])
			res.send(response)
			res.end()
		}
	})
})

/**
* route to delete a ship that is saved in the fleet
* @param ship		the name of the ship that is requested to be deleted
* @return		status 201 and the response body if the ship is deleted
* @return		error message if there the promise is rejected
*/
server.del('/fleet', function(req, res) {
	const ship = req.body.ship
	model.deleteship(ship, req, (err, response) => {
		res.setHeader('content-type', 'application/json')
		res.setHeader('accepts', 'GET, DELETE')
		if(err) {
			res.send(err)
			res.end()
		} else {
			res.status(htmlstatus['OK'])
			res.send(response)
			res.end()
		}
	})
})

/**
* route to add a ship to the fleet that is retrived by id from swapi
* @param ship		the id of the ship that is wished to be saved from swapi
* @return		status 201 and the response body if the ship is retrieved and added
* @return		error message if there the promise is rejected
*/
server.post('/fleet/:shipadd', function(req, res) { //curl -X POST http://localhost:8000/fleet/12
	const shiptofleet = req.params.shipadd
	model.addship(shiptofleet, req, (err, response) => {
		res.setHeader('content-type', 'application/json')
		res.setHeader('accepts', 'GET, POST')
		if(err){
			res.send(err)
			res.end()
		} else {
			res.status(htmlstatus['created'])
			res.send(response)
			res.end()
		}
	})
})

/**
* route to add a comment to a ship shared in the fleet specified by the name og the ship
* @param ship		the name of the ship that is to be updated
* @param update		the update that is to be set on the ship
* @return		status 201 and the response body if the ship is found and edited
* @return		error message if there the promise is rejected
*/
server.put('/fleet', function(req, res) {
	const ship = req.body.ship
	const update = req.body.comment
	model.updateship(ship, update, req, (err, response) => {
		res.setHeader('content-type', 'application/json')
		res.setHeader('accepts', 'GET, PUT')
		if(err){
			res.send(err)
			res.end()
		} else {
			res.status(htmlstatus['created'])
			res.send(response)
			res.end()
		}
	})
})

/**
* route to add a comment to a ship shared in the fleet specified by the name og the ship
* @return		status 201 and the response body if the ship is found and edited
* @return		error message if there the promise is rejected
*/
server.post('/captains', (req, res) => {
	model.addCaptain(req, (err, response) => {
		res.setHeader('content-type', 'application/json')
		res.setHeader('accepts', 'GET, POST')
		if (err) {
			res.send(err)
			res.end()
		} else {
			res.status(htmlstatus['created'])
			res.send(response)
			res.end()
		}
	})
})


// ./node_modules/eslint/bin/eslint.js app.js
