
'use strict'

const mongoose = require('mongoose')

const db = {
	captain: 'BethAlice',
	pass: 'sampledb'
}
//mongoose.connect('mongodb://BethAlice:sampledb@ds031597.mlab.com:31597/sampledb')
mongoose.connect(`mongodb://${db.captain}:${db.pass}@ds031597.mlab.com:31597/sampledb`)

mongoose.Promise = global.Promise
const schema = mongoose.Schema

/**
* schema for a captain - user
* @constructor user
*/
const captainSchema = new schema({
	name: String,
	username: String,
	password: String
})

exports.Captain = mongoose.model('Captain', captainSchema)

/**
* schema for a ship in the fleet
* @constructor ship
*/
const shipSchema = new schema({
	account: String,
  name: String,
  model: String,
  manufacturer: String,
  cost_in_credits: Number,
  length: String,
  max_atmosphering_speed: String,
  crew: String,
  passengers: String,
  cargo_capacity: String,
  consumables: String,
  hyperdrive_rating: String,
  MGLT: String,
  starship_class: String,
	number_infleet: Number,
  comment: String,
	url: String
})

exports.fleet = mongoose.model('fleet', shipSchema)

// ./node_modules/eslint/bin/eslint.js ./schema/schema.js
