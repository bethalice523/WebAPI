------------
Sample urls
------------

To view all ships
  GET host/ships?page=pagetobefound
  GET host/ships?page=3

To search for a ship
  GET host/ships?search=searchterm
  GET host/ships?search=destroyer

To view an individual ship
  GET host/ships/:shipid
  GET host/ships/2


To get the ships in your saved fleet
  GET host/fleet

To get an individual ship in your fleet
  GET host/fleet/nameofship
  GET host/fleet/Y-wing

To add a ship to your fleet
  POST host/fleet/idofship
  POST host/fleet/2

To delete a ship from the fleet
  DELETE host/fleet/nameofship
  DELETE host/fleet/Y-wing

To add a comment to a ship in the fleet
  PUT host/fleet/nameofship/comment
  PUT host/fleet/Y-wing/hello

To add an account
  POST host/captains
