'use strict'

const swapi = require('./modules/swapi.js')
// const baselogic = require('./modules/baselogic.js')
const mongodata = require('./modules/mongodata.js')
const auth = require('./modules/Auth.js')

const zero = 0
/**
* model for getting an individual ship
* @param {int} shipid 		the id od the ship that is to be retrieved from swapi api
* @param  {function} callback callback function
* @return	{function} callback
* @return {string}		error message if there the promise is rejected
*/
exports.getShip = (shipid, callback) => {
	swapi.getShip(shipid).then((response) => {
		return callback(null, response)
	}).catch ( err => {
		return callback(err)
	})
}

/**
* model for retrieving a page of ships from swapi api
* @param {int} page		the page number that should be returned
* @param {string} search		the name of the ship that is being searched for
* @param  {function} callback callback function
* @return {JSON}		response object if the page is found
* @return	{JSON}	response object if there are ships that satisfy the search
* @return	{string}	error message if err
*/
exports.allShip = (page, search, callback) => {
	if (search === undefined) {
		swapi.allShip(page).then((response) => {
  		return callback(null, response)
  	}).catch ( err => {
  		return callback(err)
  	 })
	}
	if (page === undefined) {
		swapi.searchShip(search).then((response) => {
  		return callback(null, response)
  	}).catch ( err => {
  		return callback(err)
  	})
	}
}

/**
* model to get the fleet of ship saved
* @param {string} request  the request object
* @param {function} callback  callback function
* @return	{JSON}	response object if the fleet is found
* @return	{string}	error message if err
*/
exports.viewfleet = (request, callback) => {
	auth.getHeaderCredentials(request).then( credentials => {
		this.username = credentials.username
		this.password = credentials.password
		return auth.hashPassword(credentials)
	}).then( credentials => {
		return mongodata.getCredentials(credentials)
	}).then( account => {
		const hash = account[zero].password
		return auth.validatePassword(this.password, hash)
	}).then( () => {
		return mongodata.viewfleet(this.username)
	}).then( fleet => {
		callback(null, fleet)
	}).catch( err => {
		callback(err)
	})
}

/**
* model to retrieve an individual ship from the fleet
* @param {string} ship		the name of the ship to be retrieved
* @param  {string} request the request object
* @param  {function} callback callback function
* @return	{JSON}	response body if the ship specified is found in the fleet
* @return	{string}	error message if err
*/
exports.viewindividual = (ship, request, callback) => {
	auth.getHeaderCredentials(request).then( credentials => {
		this.username = credentials.username
		this.password = credentials.password
		return auth.hashPassword(credentials)
	}).then( credentials => {
		return mongodata.getCredentials(credentials)
	}).then( account => {
		const hash = account[zero].password
		return auth.validatePassword(this.password, hash)
	}).then( () => {
		return mongodata.viewindividual(ship, this.username)
	}).then( ship => {
		callback(null, ship)
	}).catch( err => {
		callback(err)
	})
}


/**
* model to add a ship to the fleet that is retrived by id from swapi
* @param {string} shipid		the id of the ship that is wished to be saved from swapi
* @param  {string} request the request object
* @param  {function} callback callback function
* @return {JSON}		the response body if the ship is retrieved and added
* @return {string}		error message if err
*/
exports.addship = (shipid, request, callback) => {
	auth.getHeaderCredentials(request).then( credentials => {
		this.username = credentials.username
		this.password = credentials.password
		return auth.hashPassword(credentials)
	}).then( credentials => {
		return mongodata.getCredentials(credentials)
	}).then( account => {
		const hash = account[zero].password
		return auth.validatePassword(this.password, hash)
	}).then( () => {
		return swapi.getShip(shipid)
	}).then( (ship) => {
 		this.ship = ship
  	return mongodata.infleet(this.ship, this.username)
	}).then(() => {
		this.ship.account = this.username
		return mongodata.addship(this.ship, shipid)
	}).then( ship => {
		delete ship.account
		callback(null, ship)
	}).catch( err => {
		callback(err)
	})
}


/**
* model to delete a ship that is saved in the fleet
* @param {string} ship		the name of the ship that is requested to be deleted
* @param  {string} request the request object
* @param  {function} callback callback function
* @return {JSON}		response body if the ship is deleted
* @return	{string}	error message if err
*/
exports.deleteship = (ship, request, callback) => {
	auth.getHeaderCredentials(request).then( credentials => {
		this.username = credentials.username
		this.password = credentials.password
		return auth.hashPassword(credentials)
	}).then( credentials => {
		return mongodata.getCredentials(credentials)
	}).then( account => {
		const hash = account[zero].password
		return auth.validatePassword(this.password, hash)
	}).then( () => {
		return mongodata.deleteship(ship, this.username)
	}).then( ship => {
		callback(null, ship)
	}).catch( err => {
		callback(err)
	})
}

/**
* model to add a comment to a ship shared in the fleet specified by the name og the ship
* @param {string} ship		the name of the ship that is to be updated
* @param {string} update		the update that is to be set on the ship
* @param  {string} request the request object
* @param  {function} callback callback function
* @return {JSON}		response body if the ship is found and edited
* @return	{string}	error message if err
*/
exports.updateship = (ship, update, request, callback) => {
	auth.getHeaderCredentials(request).then( credentials => {
		this.username = credentials.username
		this.password = credentials.password
		return auth.hashPassword(credentials)
	}).then( credentials => {
		return mongodata.getCredentials(credentials)
	}).then( account => {
		const hash = account[zero].password
		return auth.validatePassword(this.password, hash)
	}).then( () => {
		return mongodata.updateship(ship, update, this.username)
	}).then( ship => {
		callback(null, ship)
	}).catch( err => {
		callback(err)
	})
}

/**
* model to add a captain user
* @param {object} request		the contents of the request
* @param  {function} callback callback function
* @return {JSON}		response body if the captain is created
* @return	{string}	error message if err
*/
exports.addCaptain = (request, callback) => {
	let data
	auth.getHeaderCredentials(request).then( credentials => {
		return auth.hashPassword(credentials)
	}).then( credentials => {
		data = credentials
		return mongodata.accountDuplicate(credentials)
	}).then( () => {
		return extractBodyKey(request, 'name')
	}).then( name => {
		data.name = name
		return mongodata.addCaptain(data)
	}).then( data => {
		callback(null, data)
	}).catch( err => {
		callback(err)
	})
}

//-------
/**
* function for extracting the key from the request
* @param {object} request the request object
* @param {object} key the key to the authentication
* @return {string} error messahge if the key is missing
* @return {object} the request body key
*/
const extractBodyKey = function(request, key) {
	return new Promise( (resolve, reject) => {
		if (request.body === undefined || request.body[key] === undefined) {
			reject(new Error('missing key'))
		} else {
			resolve(request.body[key])
		}
	})
}

// ./node_modules/eslint/bin/eslint.js model.js
