'use strict'
const mongodb = require('mongodb')
// const swapi = require('./swapi.js')
// const baselogic = require('./baselogic.js')
const schema = require('../schema/schema.js')

// const uri = 'mongodb://BethAlice:sampledb@ds031597.mlab.com:31597/sampledb'


/**
* function that will return the whole fleet from the data storage
* @param {string} captain	the current user
* @return	{string} err	error message if the connection to database fails
* @return	{string} err	error message if the fleet is empty
* @return	{JSON} doc	the ships in the fleet
*/
exports.viewfleet = function(captain) {
	return new Promise( (resolve, reject) => {
		schema.fleet.find({account: captain}, (err, docs) => {
			if (err) {
				reject(new Error('error with database'))
			}
  		if (!docs.length) {
			reject(new Error('fleet empty'))
		} else {
			resolve(docs)
		}
    	})
  	})
}

/**
* function that will view an individual ship specified by the name of the ship
* @param {string} ship		the name of the ship that should be found from the fleet
* @param {string} captain		the current user
* @return	{string} err	error message if the connection to database fails
* @return	{string} err	error message if there is no ship defined
* @return	{string} err	error message if the specified ship cannot be found
* @return	{JSON} doc	the ships that has been searched for
*/
exports.viewindividual = function(ship, captain) {
	return new Promise( (resolve, reject) => {
		if (ship === '') {
			reject(new Error('400 no ship is defined'))
		}
		schema.fleet.find({account: captain, name: ship}, (err, docs) => {
			if (err) {
				reject(new Error('error with database'))
			}
  		if (!docs.length) {
			reject(new Error('404 ship cannot be found in fleet'))
		} else {
			resolve(docs)
		}
    	})
  	})
}

/**
* function for adding the ship to the fleet
* @param {JSON} ship		this contains the data in JSON format to be added to the fleet
* @param {string} shipid		id of the ship to be added
* @return	{string} err	error message if the connection to database fails
* @return	{string} err	error message if there is no ship defined
* @return	{JSON} resolve 	the ship that has been added to the fleet
*/
exports.addship = function(ship, shipid) {
	return new Promise( (resolve, reject) => {
		if(shipid === '') reject(new Error('400 no shipid is defined'))
		if(ship === '') reject(new Error('400 no ship is defined'))
		const fleetship = new schema.fleet(ship)
  	fleetship.save( (err, fleetship) => {
  		if (err) {
			reject(new Error('error saving'))
		} else {
  		  resolve(fleetship)
		}
  	})
	})
}

/**
* deletes the ship specified in the arguments
* @param {string} shipToDelete		the name of the ship that is wished to be deleted
* @param {string} captain		the current user
* @return {string} err		error message if the connection to mlab fails
* @return	{string} err	error message if no ship is defined
* @return {string} resolve		success message if the ship is deleted
*/
exports.deleteship = function(shipToDelete, captain) {
	return new Promise( (resolve, reject) => {
		if(shipToDelete === '') reject(new Error('no ship is defined'))
		schema.fleet.remove({account: captain, name: shipToDelete}, (err) => { // :dev docs removed
			if (err) {
				reject(new Error('error with database'))
			} else {
  		//if (!docs.length) reject(new Error('ship is not in fleet'))
				resolve('ship has been deleted')
			}
    	})
  	})
}

/**
* will update the ship with comments if the user wants to add them
* @param {string} ship		the ship that is specified to be updated
* @param {string} update		the comment that is wished to be added to the ship
* @param {string} captain		the current user
* @return	{string} err	error message if the connection to database fails
* @return	{string} err	error message if there is no ship defined
* @return	{string} err	error message if there is no update defined
* @return	{string} err	error message if the ship specified cannot be found
* @return	{JSON} resolve	updated ship
*/
exports.updateship = function(ship, update, captain){
	const uri = 'mongodb://BethAlice:sampledb@ds031597.mlab.com:31597/sampledb'
	return new Promise ((resolve, reject) => {
		if (ship === undefined) {
			reject(new Error('400 no ship is defined'))
		}
		if (update === undefined){
			reject(new Error('400 no comment is defined'))
		} else {
  		mongodb.MongoClient.connect(uri, function(err, db) {
  			if(err) throw err
  			const fleet = db.collection('fleets')
  			fleet.update(
              	  { 'account': captain, 'name': ship },
                	{ $set: {'comment': update} },
                  { upsert: true }
              	)
  			resolve(ship + ' updated')

  			db.close(function(err) {
  				if(err) throw err
  			})
  		})
		}
	})
}
// exports.updateship = function(ship, update, captain) {
//   console.log(ship)
//   console.log(update)
//   console.log(captain)
//   return new Promise( (resolve, reject) => {
//       schema.fleet.update(
//       	  { "account": captain, "name": ship },
//         	{ $set: {"comment": update} },
//           { upsert: true }
//       	)
//       resolve(ship)
//   	})
// }

/**
* function for checking if the ship is already in the fleet
* @param {string} shipsearch    the ship to be searched for
* @param {string} captain    the current user
* @return {string} 	error message if the connection to mlab fails
* @return {string} error message if already in database
*/
exports.infleet = function(shipsearch, captain) {
	const ship = shipsearch.name
	return new Promise( (resolve, reject) => {
  	schema.fleet.find({account: captain, name: ship}, (err, docs) => {
  		if (err) {
			reject(new Error('error with database'))
		}
		if (docs.length) {
			reject(new Error('ship already in fleet'))
		} else {
  		  resolve()
		}
  	})
	})
}

/**
* will add a new user to the system
* @param {object} captaininfo		The object passed in constaining authentication and user
* @return	{string} 	error message if the user is invalid
* @return	{string} 	error message if cannot create the account
* @return	{string} resolve	success message if the captain is successfully added
*/
exports.addCaptain = function(captaininfo) {
	return new Promise( (resolve, reject) => {
  	if (!'username' in captaininfo && !'password' in captaininfo && !'name' in captaininfo) {
  		reject(new Error('invalid user'))
  	}
  	const captain = new schema.Captain(captaininfo)
  	captain.save((err) => { // :dev removed captain
  		if (err) {
  			reject(new Error('error creating account'))
  		}
  		delete captaininfo.password
  		resolve('added the new captain')
  	})
	})
}

/**
* will check for a duplicate user
* @param {object} captain		The object passed in constaining details on the captain
* @return	{string} err	error message if the user already exists
*/
exports.accountDuplicate = function(captain) {
	return new Promise( (resolve, reject) => {
  	schema.Captain.find({username: captain.username}, (err, docs) => {
		if(err) {
			reject(new Error('error with database'))
		}
  		if (docs.length) {
			     reject(new Error('username already exists'))
		  } else {
  		  resolve()
		  }
  	})
	})
}

/**
* will get the users credentials
* @param {object} credentials		The object passed in constaining details on the captain
* @return	{string} 	error message if the user cannot be found
* @return	{object} docs	if the user credentials have been found
*/
exports.getCredentials = function(credentials) {
	return new Promise( (resolve, reject) => {
  	schema.Captain.find({username: credentials.username}, (err, docs) => {
  		if (err) {
			reject(new Error('404 user cannot be found'))
		}
  		if (docs.length) {
			resolve(docs)
		}
  	})
	})
}

// ./node_modules/eslint/bin/eslint.js ./modules/mongodata.js
