'use strict'
const swapi = require('swapi-node')
const swapiURL = 'http://swapi.co/api/starships/'

/**
* Gets a ship by its id from swapi api
* @param {int} shipid   the ID of the ship to be found
* @return {string} message  error message 'this is not a valid id' if the shipid param is not a number
* @return {string} message  error message 'could not retrieve' if the ship specified by shipid cannot be found
* @return {JSON} result   the result body if the ship has been found
*/
exports.getShip = function(shipid) {
	return new Promise ((resolve, reject) => {
		if (isNaN(shipid)) {
			const message = 'this is not a valid id'
			reject(message)
		} else {
			const newURL = swapiURL.concat(shipid)
			swapi.get(newURL).then(function(result, err) {
				if (err) {
					const message = '404 could not retrieve'
					reject(message)
				} else {
					resolve(result)
				}
			})
		}
	})
}

/**
* Gets all the ship by page from swapi api
* If a page number is specified then it will get that page
* If a page number is not specified it wil automatically get the first page
* @param {int} page   The page to return fromw swapi api
* @return {string} message   error message 'could not retrieve' if the page of ships cannot be retrieved
* @return {JSON} result  the resulting body if successful
*/
exports.allShip = function(page) {
	const pagenumber = '?page='.concat(page)
	const newURL = swapiURL.concat(pagenumber)
	return new Promise ((resolve, reject) => {
		if (page === '') reject(new Error('page is not defined'))
		swapi.get(newURL).then(function(result, err) {
  		if (err) {
  			const message = '404 could not retrieve'
  			reject(message)
  		} else {
  			resolve(result)
  		}
  	})
	})
}

/**
* Will search for a ship by the name specified rather that by id
* @param {int} searchid   the name of the ship that the user wishes to search for
* @return {string} message   error message 'could not retrieve' if the ship searched for does not exist
* @return {JSON} result  the result body with the search results if there are satisfying ships
*/
exports.searchShip = function(searchid) {
	const swapisearchURL = 'http://swapi.co/api/starships/?search='
	const newURL = swapisearchURL.concat(searchid)
	return new Promise ((resolve, reject) => {
		if (searchid === '') reject(new Error('search item is not defined'))
  	swapi.get(newURL).then(function(result, err) {
  		if (err) {
  			const message = '404 could not retrieve'
  			reject(message)
  		} else {
  			resolve(result)
  		}
  	})
	})
}
