'use strict'

// const test = {'name': 'Star Destroyer',
//               'model': 'Imperial I-class Star Destroyer',
//               'manufacturer': 'Kuat Drive Yards',
//               'cost_in_credits': '150000000',
//               'length': '1,600',
//               'max_atmosphering_speed': '975',
//               'crew': '47060',
//               'passengers': '0',
//               'cargo_capacity': '36000000',
//               'consumables': '2 years',
//               'hyperdrive_rating': '2.0',
//               'MGLT': '60',
//               'starship_class': 'Star Destroyer',
//               'pilots': [],
//               'films': ['http://swapi.co/api/films/3/',
//               'http://swapi.co/api/films/2/',
//               'http://swapi.co/api/films/1/'],
//               'created': '2014-12-10T15:08:19.848000Z',
//               'edited': '2014-12-22T17:35:44.410941Z',
//               'url': 'http://swapi.co/api/starships/3/'}


/**
* function for parsing the user entered data into the corect JSON format
* @param {string} stringtomanipulate   the string that is passed in to be converted to relevant JSON
* @param {string} entryname    the name of the entry to be parsed to relevant JSON
* @return {JSON} jsonstring   the new relevant JSON
*/
exports.changetoJSON = function(stringtomanipulate, entryname) {
	const stringwithformat = '{"' +entryname +'": "' +stringtomanipulate + '"}'
	const jsonstring = JSON.parse(stringwithformat)
	return(jsonstring)
}

const createLink = function(ship) {
	const href = "http://swapi.co/api/starships/" +ship + '/'
	return(href)
}

/**
* function for converting the raw swapi data into more relevant data to save to the fleet
* @param {JSON} JSONfile   the swapi raw data to be converted
* @return {JSON} processed   return the new processed ship to be saved to the fleet
*/
exports.tidyingdata = function (JSONfile, shipid, req, res) {
	const href = createLink(shipid)

    var processed = []
        var thisObj = {}

        thisObj["name"] = JSONfile["name"]
        thisObj["model"] = JSONfile["model"]
        thisObj["manufacturer"] = JSONfile["manufacturer"]
        thisObj["cost_in_credits"] = JSONfile["cost_in_credits"]
        thisObj["length"] = JSONfile["length"]
        thisObj["max_atmosphering_speed"] = JSONfile["max_atmosphering_speed"]
        thisObj["crew"] = JSONfile["crew"]
        thisObj["passengers"] = JSONfile["passengers"]
        thisObj["cargo_capacity"] = JSONfile["cargo_capacity"]
        thisObj["consumables"] = JSONfile["consumables"]
        thisObj["hyperdrive_rating"] = JSONfile["hyperdrive_rating"]
        thisObj["MGLT"] = JSONfile["MGLT"]
        thisObj["starship_class"] = JSONfile["starship_class"]
				thisObj["links"] = {"ref" : "origin",
														"href" : href}

      processed.push(thisObj)
      return(processed)
}
