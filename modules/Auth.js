'use strict'
const bcrypt = require('bcrypt')

/**
* will get the user credentials from the header
* @param {object} request		The object passed in constaining authentication and user
* @return	{string} err	error message if the authorization header is missing
* @return	{string} err	error message if the username is missing
* @return	{string} err	error message if the password is missing
* @return	{string} resolve	username and password extracted
*/
exports.getHeaderCredentials = function(request) {
	return new Promise((resolve, reject) => {
  	if (request.authorization === undefined || request.authorization.basic === undefined) {
  		reject(new Error('401 authorization header missing'))
  	}
  	const auth = request.authorization.basic
  	if (auth.username === undefined) {
  		reject(new Error('401 missing username'))
  	}
		if(auth.password === undefined ) {
			reject(new Error('401 missing password'))
		}
  	resolve({username: auth.username, password: auth.password})
	})
}

/**
* will hash the password so that it is encrypted
* @param {object} credentials   the credentials containing the password to be hashed
* @return {object} credentials    the new hashed credentials
*/
exports.hashPassword = function(credentials) {
	return new Promise((resolve) => {
		const saltval = 10
		const salt = bcrypt.genSaltSync(saltval)
		credentials.password = bcrypt.hashSync(credentials.password, salt)
		resolve(credentials)
	})
}

/**
* will validate the password entered
* @param {string} provided    the password provided by the user
* @param {string} stored    the password stored for the user
* @return {string} error message if password is incorrect
*/
exports.validatePassword = function(provided, stored) {
	return new Promise( (resolve, reject) => {
		if (!bcrypt.compareSync(provided, stored)) {
  		reject(new Error('incorrect password'))
  	} else {
			resolve()
		}
	})
}

// ./node_modules/eslint/bin/eslint.js ./modules/Auth.js
