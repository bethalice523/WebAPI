'use strict'
// const assert = require('assert')
// const should = require('should')
const swapimodule = require('./../modules/swapi.js')
// const swapi = require('swapi-node')
// const swapiURL = 'http://swapi.co/api/starships/'
//const samplevariables = require('./samplevariables.js')

describe('swapi', function() {

	describe('1. Function - getShip', function() {
		it('test to see if when entering a NaN it returns not a valid id error',(done) => {
			swapimodule.getShip('b').catch( err => {
				const expectedResult = 'this is not a valid id'
				expect(err).toBe(expectedResult)
				done()
			})
		})
		// it('test to see if passed an empty parameter it will return error',(done) => {
		// 	swapimodule.getShip('').catch( err => {
		// 		const expectedResult = 'could not retrieve'
		// 		expect(err).toBe(expectedResult)
		// 		done()
		// 	})
		// })

		// it('test to see it returns the expected result',(done) => {
		// 	swapimodule.getShip('2').then((response) => {
		// 		expect(response).toBe(samplevariables.individualship)
		// 		done()
		// 	})
		// })
		it('test to see it returns the expected result',(done) => {
			swapimodule.getShip('2').then((response) => {
				expect(response.name).toBe('CR90 corvette')
				expect(response.MGLT).toBe('60')
				done()
			})
		})
		it('should be in JSON format',(done) => {
			swapimodule.getShip('2').then((response) => {
				expect(response).toEqualJson
				done()
			})
		})
	})

	describe('2. Function - allShip', function() {
		it('should be in JSON format',(done) => {
			swapimodule.allShip('1').then((response) => {
				expect(response).toEqualJson
				done()
			})
		})
		it('test to see it will return the expected page when given query 1',(done) => {
			swapimodule.allShip('3').then((response) => {
				expect(response.next).toBe("http://swapi.co/api/starships/?page=4")
				expect(response.previous).toBe("http://swapi.co/api/starships/?page=2")
				done()
			}).catch(err => {
					expect(err).toBe('404 could not retrieve')
					done()
			})
		})

		it('test to see it will return error when no page',(done) => {
			swapimodule.allShip('').catch( err => {
					expect(err.message).toBe('page is not defined')
					done()
			})
		})
	})

	describe('3. Function - searchShip', function(){
		it('test to see it will return correct search results',(done) => {
			swapimodule.searchShip('nulltest1234').then( (response) => {
				expect(response.count).toBe(0)
				expect(response.next).toBe(null)
				expect(response.previous).toBe(null)
				done()
			}).catch( err => {
				expect(err).toBe('404 could not retrieve')
				done()
			})
		})

		it('test to see it will return error when no search term', function() {
			swapimodule.searchShip('').catch( err => {
					expect(err.message).toBe('search item is not defined')
					done()
				})
			})
	})

})

// ./node_modules/.bin/jasmine tests/swapitests.js
// ./node_modules/eslint/bin/eslint.js swapitests.js

// ./node_modules/.bin/istanbul cover ./node_modules/.bin/jasmine ./spec/swapi-spec.js ./modules/swapi.js
