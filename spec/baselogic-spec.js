'use strict'
const baselogic = require('./../modules/baselogic.js')

const test = {'name': 'Star Destroyer',
              'model': 'Imperial I-class Star Destroyer',
              'manufacturer': 'Kuat Drive Yards',
              'cost_in_credits': '150000000',
              'length': '1,600',
              'max_atmosphering_speed': '975',
              'crew': '47060',
              'passengers': '0',
              'cargo_capacity': '36000000',
              'consumables': '2 years',
              'hyperdrive_rating': '2.0',
              'MGLT': '60',
              'starship_class': 'Star Destroyer',
              'pilots': [],
              'films': ['http://swapi.co/api/films/3/',
              'http://swapi.co/api/films/2/',
              'http://swapi.co/api/films/1/'],
              'created': '2014-12-10T15:08:19.848000Z',
              'edited': '2014-12-22T17:35:44.410941Z',
              'url': 'http://swapi.co/api/starships/3/'}

describe('swapi', function() {

	describe('1. Function - changetoJSON', function() {

		it('should be in JSON format',function() {
			baselogic.changetoJSON('3', 'test'),function() {
				expect(response).toEqualJson
				done()
			}
		})

		it('should be correct form',function() {
			baselogic.changetoJSON('3', 'test'),function() {
				expect(response.test).toBe(3)
				done()
			}
		})

	})

	describe('1. Function - tidyingdata', function() {

		it('should be in JSON format',function() {
			baselogic.tidyingdata(test),function() {
				expect(response).toEqualJson
				done()
			}
		})

		it('should keep certain data',function() {
			baselogic.tidyingdata(test),function() {
				expect(response.name).toBe('Star Destroyer')
				expect(response.starship_class).toBe('Star Destroyer')
				done()
			}
		})

    it('should remove certain data',function() {
      baselogic.tidyingdata(test),function() {
        expect(response.hyperdrive_rating).toBe("2.0")
        expect(response.films).toBe(null)
        done()
      }
    })

	})

})


// ./node_modules/.bin/istanbul cover ./node_modules/.bin/jasmine ./spec/baselogic-spec.js ./modules/baselogic.js
