const assert = require('assert')
const should = require('should')
const mongodata = require('./../modules/mongodata.js')
//const samplevariables = require('./samplevariables.js')
// const uri = 'mongodb://tester:tester@ds127968.mlab.com:27968/mockeddb'
const sampleupdate = [{'test': 'update'}]

const sample = { _id: '584833ebaab6220e05398042',
                name: 'Imperial shuttle',
                model: 'Lambda-class T-4a shuttle',
                manufacturer: 'Sienar Fleet Systems',
                cost_in_credits: '240000',
                length: '20',
                max_atmosphering_speed: '850',
                crew: '6',
                passengers: '20',
                cargo_capacity: '80000',
                consumables: '2 months',
                hyperdrive_rating: '1.0',
                MGLT: '50',
                starship_class: 'Armed government transport' }

const captain = { username: 'jill',
                  password: '$2a$10$uPSsxPVFprmoy/fO5ka3juv.w0lsTnTzytGxmcCfa.q9XoKhGgp7S',
                  name: 'jill' }

describe('database connection', function() {

  describe('1. Function - addship', function() {

    it('test to see if when nothing is in the ship it will throw an error',(done) =>{
      mongodata.addship('', 2).catch( err=> {
        expect(err.message).toBe('400 no ship is defined')
        done()
      })
    }) // passes
    it('test to see if when nothing is in the ship it will throw an error',(done) =>{
      mongodata.addship(sample, '').catch( err=> {
        expect(err.message).toBe('400 no shipid is defined')
        done()
      })
    }) // passes
    it('should return a success message if input is valid',(done) =>{
      mongodata.addship(sample, '22').then((response) => {
        // const expectedResult = 'adding ship'
        expect(response.name).toBe("Imperial shuttle")
        done()
      }).catch( err => {
        expect(err.message).toBe('error saving')
        done()
      })
    })

  })

  describe('2. Function - viewfleet', function() {

    it('should be in JSON format',(done) => {
			mongodata.viewfleet('beth').then((response) => {
				expect(response).toEqualJson
				done()
			}).then((response) => {
        expect(response.length).toBe(2) // assumes 2 idems in database
        done()
      })
		})
    it('should return error if fleet is empty', (done) => { // assumes nothing in database
      mongodata.viewfleet('beth').catch( err => {
        expect(err.message).toBe('fleet empty')
        done()
      })
    })

  })

    describe('3. Function - viewindividual', function() {

      it('should be in JSON format',(done) => {
        mongodata.viewindividual('Imperial shuttle', 'beth').then((response) => {
          expect(response).toEqualJson // assuming in database
          done()
        })
      })
      it('test to see it returns the expected result',(done) => {
        mongodata.viewindividual('Imperial shuttle', 'beth').then((response) => {
          expect(response.name).toBe('Imperial shuttle')
          expect(response.crew).toBe(6)
          done()
        })
      })
      it('should throw an error if no ship is defined',(done) =>{
        mongodata.viewindividual('', 'beth').catch( err=> {
          expect(err.message).toBe('400 no ship is defined')
          done()
        })
      })
      it('should throw an error if the ship to be found isnt in the fleet',(done) =>{
        mongodata.viewindividual('nonexistent', 'beth').catch( err=> {
          expect(err.message).toBe('404 ship cannot be found in fleet')
          done()
        })
      })
    })

    describe('4. Function - deleteShipInFleet', function() {

      it('should throw an error if no ship is defined',(done) =>{
        mongodata.deleteship('', 'beth').catch(err => {
          expect(err.message).toBe('no ship is defined')
          done()
        })
      })

      it('should return a success message when deleted',(done) => {
        mongodata.deleteship('Imperial shuttle', 'beth').then((response) => {
          expect(response).toBe('ship has been deleted')
          done()
        })
      })

    })

    describe('5. Function - updateShip', function() {

      it('should throw an error if no ship is defined',(done) =>{
        mongodata.updateship(undefined, 'test comment', 'beth').catch(err => {
          expect(err.message).toBe('400 no ship is defined')
          done()
        })
      })

      it('should throw an error if no update is defined',(done) =>{
        mongodata.updateship('Imperial shuttle', undefined, 'beth').catch(err => {
          expect(err.message).toBe('400 no comment is defined')
          done()
        })
      })

      it('should send a success message',(done) => {
        mongodata.updateship('Imperial shuttle', 'test comment', 'beth').then((response) => {
          expect(response).toBe('Imperial shuttle updated')
          done()
        })
      })

    })
//   })

  describe('6. Function - infleet', function() {
    it('should return error if in the fleet', (done) => {
      mongodata.infleet('Imperial shuttle', 'beth').catch(err => {
        expect(err.message).toBe('ship already in fleet')
        done()
      })
    })
  })

  describe('7. Function - addCaptain', function() {
    it('should return error if captaininfo is invalid', (done) =>{
      mongodata.addCaptain('').catch(err => {
        expect(err.message).toBe('invalid user')
      })
    })
    it('should return success message if added', (done) =>{
      var json = JSON.parse(captain);
      mongodata.addCaptain(json).then((response) => {
        expect(response).toBe('added the new captain')
      }).catch(err => {
        expect(err.message).toBe('error with database')
      })
    })
  })

  describe('8. Function - accountDuplicate', function() {
    it('should return error if already exists', (done) => {
      mongodata.accountDuplicate({'username' : 'beth'}).catch(err => {
        expect(err.message).toBe('username already exists')
      })
    })
  })

  describe('9. Function - getCredentials', function() {
    it('should return error if cant get credentials', (done) => {
      mongodata.getCredentials('').catch(err => {
        expect(err.message).toBe('404 user cannot be found')
      })
    })
  })

 })
// ./node_modules/.bin/istanbul cover ./node_modules/.bin/jasmine ./spec/mongodata-spec.js ./modules/mongodata.js
