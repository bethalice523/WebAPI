'use strict'
const authmodule = require('./../modules/Auth.js')

const credentials = { scheme: 'Basic',
                      credentials: 'YmV0aDpwYXNzd29yZA==',
                      basic: { username: 'beth',
                               password: 'password' }
                    }

const credusername = { scheme: 'Basic',
                      credentials: 'YmV0aDpwYXNzd29yZA==',
                      basic: { username: undefined,
                               password: 'password' }
                    }
const credpasswrd = { scheme: 'Basic',
                      credentials: 'YmV0aDpwYXNzd29yZA==',
                      basic: { username: 'beth',
                             password: undefined }
                    }


describe('Auth', function() {
  describe('1. Function - getHeaderCredentials', function() {
    it('test to see it will return correct credentials',(done) => {
      authmodule.getHeaderCredentials(credentials).then((response) => {
        expect(response.username).toBe('beth')
        expect(response.password).toBe('password')
        done()
      })
    })
    it('test to see it will return error if no username',(done) => {
      authmodule.getHeaderCredentials(credusername).catch( err => {
        expect(err.message).toBe('401 authorization header missing')
        done()
      })
    })
    it('test to see it will return error if no password',(done) => {
      authmodule.getHeaderCredentials(credpasswrd).catch( err => {
        expect(err.message).toBe('401 authorization header missing')
        done()
      })
    })
  })

  describe('1. Function - hashPassword', function() {
    it('test to see if it hashes password', (done) => {
      authmodule.hashPassword().then((response) => {
        expect(response.password).not.toEqual('password')
      })
    })
  })

})


// ./node_modules/.bin/istanbul cover ./node_modules/.bin/jasmine ./spec/Auth-spec.js ./modules/Auth.js
